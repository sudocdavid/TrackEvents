# TrackEvents
#### _The simpliest way to track upcoming events with your friends!_

TrackEvents is an event-tracking website that enables users to easily find and exchange their favourite events with friends.

## Features
- Select a country to view its events in a list
- List events with:
    - Title
    - Description
    - Location
    - Date
    - Ticket-Limit
    - Image
- Filter listed events by multiple parameters:
    - Genres
    - Artists
    - Locations
    - Dates
    - Own Favourites
    - Friends' Favourites
- Favourites List
    - Add events to favourites list
    - Show favourites list
- Users & Friends
    - Register and Login
    - Search for friends by username
    - Suggest events to friends

People are the creators. By coming together in events we give the world a meaning.
As [Scarlett Thomas](https://www.goodreads.com/author/show/53597.Scarlett_Thomas) once wrote in her book [Going Out](https://www.goodreads.com/book/show/13508320-going-out):

> People make events into stories. 
> Stories give events meaning.

## Technologies

TrackEvents uses multiple open source projects to provide the best user experience available:

- [Ticketmaster](https://developer.ticketmaster.com/products-and-docs/apis/getting-started/) - Our main API to retrieve event data
- [React](https://reactjs.org/docs/getting-started.html) - Main library
- [Google Places API](https://developers.google.com/maps/documentation/places/web-service/photos) - Receiving country images
- [Node.js](https://nodejs.org/en/) - Setup & Testing
- [MongoDB](https://www.mongodb.com/) - Storing our user data
- [Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/) - Fancy UI

... and for sure TrackEvents itself is open source with a public repository on [GitHub](LINK HERE).

_Big thanks to:_
[![ticketmaster](https://cdn4.impact.com/display-refbylogo-image/30588__vf96a58d.gif)](https://developer.ticketmaster.com/products-and-docs/apis/getting-started/)

## Installation

TrackEvents requires ?

## Docker

TrackEvents is very easy to install and deploy in a Docker container.

## License
MIT
