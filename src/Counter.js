import React, { useState } from 'react';
import {GuestView, UserView} from './users';

function Counter() {
    const [count, setCounter] = useState(0);

    if (count % 2 === 0) {
        return <div><button onClick={() => setCounter(count + 1)}>Klick mich!</button><GuestView /></div>
    } else {
        return (
            <div>
                <button onClick={() => setCounter(count + 1)}>Klick mich!</button>
                <p>
                    Du hast mich {count} mal geklickt
                </p>
                <UserView />
            </div>
        )
    }
}

export default Counter;