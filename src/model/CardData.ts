export class CardData {

    key: String;
    id: String;
    title: String;
    description: String;
    sub_description: String;
    href: String;
    img_src: String;
    fetch_img_query: String;
    data: any;
    fetch_img_query_alt: String;

    constructor(key: String, id: String, title: String, description: String, sub_description: String, href: String, img_src: String, fetch_img_query: String, data: any, fetch_img_query_alt: String) {
        this.key = key;
        this.id = id;
        this.title = title;
        this.description = description;
        this.sub_description = sub_description;
        this.href = href;
        this.img_src = img_src;
        this.fetch_img_query = fetch_img_query;
        this.data = data;
        this.fetch_img_query_alt = fetch_img_query_alt;
    }
}