export class IContacts {
    userId: string;
    contacts: string[];

    constructor(userId: string, contacts: string[]) {
        this.userId = userId;
        this.contacts = contacts;
    }
}