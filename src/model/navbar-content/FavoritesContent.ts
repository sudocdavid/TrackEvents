import { EventData } from "../EventData";

export class FavoritesContent {
    /**
     * The favorite event stored with the date when it was stored
     */
    events: Map<EventData, Date>;

    constructor(events: Map<EventData, Date>) {
        this.events = events;
    }
}