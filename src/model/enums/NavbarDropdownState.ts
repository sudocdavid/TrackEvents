export enum NavbarDropdownState {
    Login = 0,
    Register = 1,
    Favourites = 2,
    Contacts = 3,
    Messages = 4,
    Settings = 5,
}