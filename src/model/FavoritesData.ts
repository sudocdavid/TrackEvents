export class IFavorites {
    userId: string;
    favorites: string[];

    constructor(userId: string, favorites: string[]) {
        this.userId = userId;
        this.favorites = favorites;
    }
}