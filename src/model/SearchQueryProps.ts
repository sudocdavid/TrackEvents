export class SearchQueryProps {

    data: any[];
    placeholder: String;
    filterListItem: (item: String, input: String) => boolean;
    handleFilteredData;

    constructor(data: any[], placeholder: String, filterListItem: (item: String, input: String) => boolean, handleFilteredData: any) {
        this.data = data;
        this.placeholder = placeholder;
        this.filterListItem = filterListItem;
        this.handleFilteredData = handleFilteredData;
    }
}