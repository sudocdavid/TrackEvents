export class EventData {
    key: string;
    id: string;
    title: string;
    location: string;
    date: string;
    description: string;
    link: string;
    images: string[];
    rawData: string[];

    constructor(key: string, id: string, title: string, location: string, date: string, description: string, link: string, images: string[], rawData: string[]) {
        this.key = key;
        this.id = id;
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.link = link;
        this.images = images;
        this.rawData = rawData;
    }
}
