import React from 'react';
import { CarouselImageSlider } from "../../widgets/CarouselImageSlider";
import { HomeCardsContainer } from "../../widgets/HomeCardsContainer";
import { ImageChangeable } from '../../components/ImageChangeable';
import { SearchQueryProps } from '../../model/SearchQueryProps';
import { SelectAllButton } from '../../components/SelectAllButton';
import selected_all_icon from "../../res/images/selected_all_icon.png";
import { SearchBar } from "../../components/SearchBar";
import { convertData_CountriesToCards, fetchCountriesData, sortDataByPopulation } from '../../data/CountryData';
import { convertData_GenresToCards, fetchGenresData, getGenreRootNode } from '../../data/GenreData';

import './HomePage.scss';
import { fetchEventsData } from '../../data/EventData';
import SessionDataController from '../../data/SessionDataController';
import { changeUserCountry } from '../../data/database/axios';

const SELECT_COUNTRY = "select-country";
const SELECT_GENRE = "select-genre";
const SELECT_COUNTRY_PAGE_ID = "select-country-page";
const SELECT_GENRE_PAGE_ID = "select-genre-page";
const TRANSITION_TIME_MS = 0;

export class HomePage extends React.Component {

    _isMounted = false;
    countriesData;
    genresData;
    selectedCountry;
    selectedGenre;

    constructor(props) {
        super(props);

        this.state = {
            countriesDataFiltered: [],
            genresDataFiltered: [],
            select_state: SELECT_COUNTRY
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.initSelectPagesStyles();
        fetchCountriesData().then(data => {
            if (this._isMounted) this.setState({ countriesDataFiltered: this.countriesData = data })
        });
        fetchGenresData().then(data => {
            if (this._isMounted) this.setState({ genresDataFiltered: this.genresData = data })
        });
        if (this.state.select_state === SELECT_COUNTRY) document.getElementById(SELECT_COUNTRY_PAGE_ID).focus();
        else if (this.state.select_state === SELECT_GENRE) document.getElementById(SELECT_GENRE_PAGE_ID).focus();
    }
    componentDidUpdate = () => this.renderSelectPage();
    componentWillUnmount = () => this._isMounted = false;

    render() {
        let onSelectAll;
        if (this.state.select_state === SELECT_COUNTRY) onSelectAll = () => this.setCountry(null);
        else if (this.state.select_state === SELECT_GENRE) {
            onSelectAll = () => this.setGenre(null);
        }

        const countrySearchQueryProps = new SearchQueryProps(
            this.countriesData,
            "Which country are you looking for?",
            (item, input) => { return item.name.toLowerCase().includes(input.toLowerCase()) },
            data => { this.setState({ countriesDataFiltered: data }) }
        );

        const genreSearchQueryProps = new SearchQueryProps(
            this.genresData,
            "Which genre are you looking for?",
            (item, input) => { return getGenreRootNode(item).name.toLowerCase().includes(input.toLowerCase()) },
            data => this.setState({ genresDataFiltered: data })
        )

        if (this.state.select_state === SELECT_COUNTRY) this.activeSearchQueryProps = countrySearchQueryProps;
        else if (this.state.select_state === SELECT_GENRE) this.activeSearchQueryProps = genreSearchQueryProps;
        else this.activeSearchQueryProps = null;

        return (<>
            <CarouselImageSlider />
            <div className="quick-event-search">
                <button id="quick-event-search-button" onClick={() => this.goToSearch()}>Quick Event Search</button>
            </div>
            <div className="search-filter-section-container">
                <div className="search-filter-section">
                    {this.renderSelectedCountry()}
                    <SearchBar id="country-genre-search-bar" searchQueryProps={this.activeSearchQueryProps} />
                    <SelectAllButton onSelect={() => onSelectAll()} />
                </div>
            </div>

            <div className="select-page">
                <HomeCardsContainer id={SELECT_COUNTRY_PAGE_ID} cardsData={convertData_CountriesToCards(sortDataByPopulation(this.state.countriesDataFiltered))} selectedCard={countryData => this.setCountry(countryData)} />
                <HomeCardsContainer id={SELECT_GENRE_PAGE_ID} cardsData={convertData_GenresToCards(this.state.genresDataFiltered)} selectedCard={genreData => this.setGenre(genreData)} />
            </div>
        </>);
    }

    scrollUp() {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    resetData() {
        document.getElementById("country-genre-search-bar").value = "";
        this.setState({ countriesDataFiltered: this.countriesData, genresDataFiltered: this.genresData });
    }

    setCountry(countryData, select_state = SELECT_GENRE) {
        this.setState({ select_state: select_state });
        this.selectedCountry = countryData;
        this.scrollUp();
        this.resetData();
        let code = countryData ? countryData.data.alpha2Code : countryData;
        fetchEventsData(code);
        SessionDataController.setCountry(code);
        changeUserCountry(code);
    }

    setGenre(genreData, select_state = null) {
        this.setState({ select_state: select_state });
        this.selectedGenre = genreData;
        this.scrollUp();
        this.resetData();
        if (genreData != null && genreData.data != null) {
            SessionDataController.setGenre(genreData.data.id);
            SessionDataController.setGenreClass(genreData.sub_description + "Id");
            if (this.selectedCountry != null) {
                let detailData = {};
                detailData[genreData.sub_description + "Id"] = genreData.data.id;
                fetchEventsData(this.selectedCountry.data.alpha2Code, detailData);
            }
        } else {
            SessionDataController.setGenre(null);
            SessionDataController.setGenreClass(null);
        }
        this.goToSearch();
    }

    goToSearch() {
        setTimeout(() => {
            this.props.history.push('/search');
            setTimeout(() => this.scrollUp());
        }, TRANSITION_TIME_MS);
    }

    initSelectPagesStyles() {
        const selectCountryPage = document.getElementById(SELECT_COUNTRY_PAGE_ID);
        const selectGenrePage = document.getElementById(SELECT_GENRE_PAGE_ID);
        selectCountryPage.style.transition = TRANSITION_TIME_MS + "ms ease-in-out";
        selectGenrePage.style.transition = TRANSITION_TIME_MS + "ms ease-in-out";
    }

    renderSelectPage() {
        let stylesActive = document.getElementById(SELECT_COUNTRY_PAGE_ID).style;
        let stylesInactive = document.getElementById(SELECT_GENRE_PAGE_ID).style;
        if (this.state.select_state === SELECT_GENRE) [stylesActive, stylesInactive] = [stylesInactive, stylesActive];
        stylesInactive.opacity = 0;
        stylesInactive.zIndex = 1;
        setTimeout(() => {
            stylesInactive.position = "absolute";
            stylesInactive.maxHeight = "0";
            stylesInactive.overflow = "hidden";
            stylesInactive.visibility = "hidden";
            if (this.state.select_state == null) return;
            stylesActive.overflow = "unset";
            stylesActive.maxHeight = "unset";
            stylesActive.position = "relative";
            stylesActive.visibility = "visible";
            stylesActive.opacity = 1;
            stylesActive.zIndex = 2;
        }, TRANSITION_TIME_MS);
    }

    renderSelectedCountry = () => {
        if (this.state.select_state === SELECT_COUNTRY) {
            return <div className="country-changeable-placeholder"></div>;
        }
        const img_src = this.selectedCountry != null ? this.selectedCountry.data.flag : selected_all_icon;
        const alt_text = "Change selected country (" + (this.selectedCountry != null ? this.selectedCountry.data.alpha2Code : "ALL") + ")";
        return <ImageChangeable img_src={img_src} onClick={() => this.setCountry(null, SELECT_COUNTRY)} img_alt={alt_text} />
    }

    // includes smooth transition
    switchVisibility(stylesActive, stylesInactive) {
        stylesInactive.opacity = 0;
        stylesInactive.zIndex = 1;
        setTimeout(() => {
            stylesInactive.position = "absolute";
            stylesInactive.display = "none";
            stylesActive.display = "block";
            stylesActive.position = "relative";
            stylesActive.opacity = 1;
            stylesInactive.zIndex = 2;
        }, TRANSITION_TIME_MS);
    }

}