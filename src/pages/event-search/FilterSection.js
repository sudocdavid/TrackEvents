import React from 'react';
import './FilterSection.scss';

export class FilterSection extends React.Component {

    // TODO: not finished yet
    render() {
        return <div id="filter-section">
            <h4>Filter</h4>
            <div id="filter-section-countries">
                <h5 id="filter-section-title">Change Country</h5>
                <input id="filter-section-countries-input" placeholder="e.g. Germany" />
            </div>
            <div id="filter-section-genres">
                <h5 id="filter-section-title">Change Genre</h5>
                <input id="filter-section-countries-input" placeholder="e.g. Sports" />
            </div>
            <div id="filter-section-event-name">
                <h5 id="filter-section-title">Event Search</h5>
                <p>search for an event name ore keyword</p>
                <input id="filter-section-countries-input" placeholder="e.g. Hacking" />
            </div>
            <div id="filter-section-dates">
                <h5 id="filter-section-title">Date</h5>
                <input id="filter-section-countries-input" placeholder="e.g. Sports" type="date" />
                <p id="filter-section-date-text">to</p>
                <input id="filter-section-countries-input" placeholder="e.g. Sports" type="date" />
            </div>
            <div id="filter-section-city">
                <h5 id="filter-section-title">City</h5>
                <input id="filter-section-countries-input" placeholder="e.g. Venice" />
            </div>

            <button id="filter-section-button">search</button>
        </div>
    }

}