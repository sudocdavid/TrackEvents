import React from 'react';
import { EventCardsContainer } from '../../widgets/EventCardsContainer';
import { FilterSection } from './FilterSection';

import './EventSearchPage.scss';

export class EventSearchPage extends React.Component {
    render() {
        return (<div id="event-search-page">
            <div id="event-search-filter-section">
                <FilterSection></FilterSection>
            </div>
            <div id="event-search-cards-section">
                <EventCardsContainer cardsData={[]} />
            </div>
        </div>
        )
    }

}