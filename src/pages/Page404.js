import React from 'react';
import { Link } from 'react-router-dom';

import './Page404.scss';

export class Page404 extends React.Component {

    render() {
        return (
            <div className="div-404">
                <h2>404: Page not found</h2>
                <Link to="/home">go back home</Link>
            </div>
        )
    }
}