import React from 'react';
import { CardSelectable } from '../components/cards/CardSelectable.js';
import "./HomeCardsContainer.scss";

const CARDS_PER_RELOAD = 5;
const MAX_CARDS_NUM = 55;
const SCROLL_OFFSET = 200; // remaining pixel on bottom of page to reload more cards

function getNextCardsCount(cardsCount) {
    return Math.min(cardsCount + CARDS_PER_RELOAD, MAX_CARDS_NUM);
}

export class HomeCardsContainer extends React.Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            cardsCount: CARDS_PER_RELOAD,
            selectedCard: null
        };
    }

    componentDidMount = () => {
        this._isMounted = true;
        window.addEventListener('scroll', this.handleScroll);
        const resizeObserver = new ResizeObserver(() => this.handleBodyGreaterThanRoot());
        resizeObserver.observe(document.body)
    }
    
    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
        this._isMounted = false;
    }

    handleScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
            this.updateCardsCount();
        }
    }

    handleBodyGreaterThanRoot() {
        if (document.body.clientHeight > document.getElementById("root").clientHeight) {
            setTimeout(() => {
                if (this._isMounted && this.updateCardsCount()) {
                    this.handleBodyGreaterThanRoot();
                }
            }, 20);
        }
    }

    updateCardsCount() {
        if (this.state.cardsCount < MAX_CARDS_NUM) {
            this.setState({ cardsCount: getNextCardsCount(this.state.cardsCount) });
            return true;
        }
        return false;
    }

    render() {
        let cardsData = this.props.cardsData;
        let cards = [];
        for (let n = 0; n < Math.min(cardsData.length, this.state.cardsCount, MAX_CARDS_NUM); n++) {
            cards.push(<CardSelectable cardModel={cardsData[n]} key={cardsData[n].key} onSelect={() => this.props.selectedCard(cardsData[n])} />);
        }
        return (
            <div id={this.props.id} className='home-cards'>{cards}</div>
        );
    }

}