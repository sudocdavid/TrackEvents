import React from 'react';
import { EventCard } from '../components/cards/EventCard';
import { convertData_EventsToCardsData, fetchEventsData } from '../data/EventData';
import SessionDataController from '../data/SessionDataController';
import { EventData } from '../model/EventData';
import "./EventCardsContainer.scss";

const MAX_CARDS_NUM = 50;
const CARDS_PER_RELOAD = 5;
const SCROLL_OFFSET = 200; // remaining pixel on bottom of page to reload more cards

function getNextCardsCount(cardsCount: number) {
    return Math.min(cardsCount + CARDS_PER_RELOAD, MAX_CARDS_NUM);
}

type Props = typeof EventCard.defaultProps & {
    id: string;
    cardsData: EventData[];
}

type State = {
    eventCardsData: EventData[];
    cardsCount: number;
    selectedCard: EventData | null;
    eventInfoContainer: any;
};

export class EventCardsContainer extends React.Component<Props, State> {

    _isMounted = false;

    constructor(props: Props) {
        super(props);

        this.state = {
            eventCardsData: [],
            cardsCount: 10,
            selectedCard: null,
            eventInfoContainer: null
        };
    }

    componentDidMount = () => {
        this._isMounted = true;
        this.handleEventsData();
        window.addEventListener('scroll', this.handleScroll);
        const resizeObserver = new ResizeObserver(() => this.handleBodyGreaterThanRoot());
        resizeObserver.observe(document.body)
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleScroll);
        this._isMounted = false;
    }

    handleEventsData() {
        const genreClass = SessionDataController.getGenreClass();
        if (genreClass != null) {
            const genreData: any = {};
            genreData[genreClass] = SessionDataController.getGenre();
            fetchEventsData(SessionDataController.getCountry(), genreData).then(data => {
                return convertData_EventsToCardsData(data);
            }).then(eventCardsData => {
                this.setState({ eventCardsData: eventCardsData });
            })
        }
    }

    updateCardsCount() {
        if (this.state.cardsCount < MAX_CARDS_NUM) {
            this.setState({ cardsCount: getNextCardsCount(this.state.cardsCount) });
            return true;
        }
        return false;
    }

    handleScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - SCROLL_OFFSET) {
            this.updateCardsCount();
        }
    }

    handleBodyGreaterThanRoot() {
        const root = document.getElementById("root");
        if (root != null && document.body.clientHeight > root.clientHeight) {
            setTimeout(() => {
                if (this._isMounted && this.updateCardsCount()) {
                    this.handleBodyGreaterThanRoot();
                }
            }, 20);
        }
    }

    render() {
        let cardsData: EventData[] = this.state.eventCardsData;//this.props.cardsData;
        let cards = [];
        for (let n = 0; n < Math.min(cardsData.length, this.state.cardsCount, MAX_CARDS_NUM); n++) {
            cards.push(<EventCard key={cardsData[n].id} eventData={cardsData[n]} onSelect={() => null} onHover={(isActive: boolean) => this.updateEventInfo(isActive, cardsData[n])} />);
        }
        return (
            <div id={this.props.id} className='event-cards'>
                {cards}
                {this.state.eventInfoContainer}
            </div>
        );
    }

    updateEventInfo(isActive: boolean, eventData: EventData) {
        if (isActive) {
            this.setState({ eventInfoContainer: <></> }) // TODO: set container here (later)
        } else {
            this.setState({ eventInfoContainer: null })
        }
    }

}