import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import placeholder_image from "../res/images/placeholder.png";
import { ImageLinkButton } from '../components/ImageLinkButton';

import 'bootstrap/dist/css/bootstrap.min.css';
import './CarouselImageSlider.scss';

const IMAGE_PRE = "https://images.unsplash.com/photo-";
const IMAGE_SETTINGS = "?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1920&q=80";

const DEFAULT_DATA = [
    {
        img_src: "1604910136827-7ee8c1235c82",
        description: "Find your favourite events all over the world.",
        sub_description: "- TrackEvents"
    },
    {
        img_src: "1604156788872-fa65cd2575f5",
        description: "People are the creators. By coming together in events we give the world a meaning.",
        sub_description: "- TrackEvents"
    },
    {
        img_src: "1533174072545-7a4b6ad7a6c3",
        description: "There are as many festivals as there are mosquitoes after a rain.",
        sub_description: "- Kent Jones"
    },
    {
        img_src: "1584652868574-0669f4292976",
        description: "People make events into stories. Stories give events meaning.",
        sub_description: "- Scarlett Thomas in 'Going Out'"
    }
]

export class CarouselImageSlider extends React.Component {

    itemsData = DEFAULT_DATA;

    constructor(props) {
        super(props);

        if (this.props.images != null) {
            this.itemsData = this.props.images.map(item => {
                return { img_src: item };
            });
        }
    }

    render() {
        const carouselItems = [];
        for (const itemData of this.itemsData) {
            const img_src = this.props.images != null ? itemData.img_src : IMAGE_PRE + itemData.img_src + IMAGE_SETTINGS;
            carouselItems.push(
                <Carousel.Item className="carousel-item" key={itemData.img_src}>
                    <img
                        className="d-block w-100"
                        src={img_src}
                        alt={placeholder_image}
                    />
                    <div className="img-overlay"></div>
                    <Carousel.Caption>
                        <h3>{itemData.description}</h3>
                        <p>{itemData.sub_description}</p>
                    </Carousel.Caption>
                    <ImageLinkButton img_src={img_src} title={this.props.images == null ? "Show Image on Unsplash" : null} />
                </Carousel.Item>
            );
        }
        // TODO: add controls again, lay button-show-on-unsplash on top
        return <Carousel fade className="carousel" prevLabel={null} nextLabel={null} controls={false}>{carouselItems}</Carousel>;
    }

}