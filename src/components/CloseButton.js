import './CloseButton.scss'

export const CloseButton = (props) => {
    return <button className="close-dropdown-button" onClick={() => props.onClose()}>
        <svg width="512" height="512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" className="close-icon">
            <g id="Layer_1">
                <title>Layer 1</title>
                <line strokeWidth="30" strokeLinecap="undefined" id="svg0" y2="448" x2="448" y1="64" x1="64" stroke="#f88397" fill="none" />
                <line strokeWidth="30" strokeLinecap="undefined" id="svg1" y2="448" x2="448" y1="64" x1="64" stroke="#f88397" fill="none" transform="rotate(90, 256, 256)" />
            </g>
        </svg>
    </button>
}