import './DeleteButton.scss';
import delete_icon from "../res/images/delete_white_48dp.svg";

export const DeleteButton = (props) => {
    return (
        <div className='delete-icon' onClick={() => props.onClick()}>
            <img className="delete-img" src={delete_icon} alt={props.img_alt} />
        </div>
    )
}