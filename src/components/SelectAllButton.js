import './SelectAllButton.scss';
import select_all_icon from "../res/images/select_all_icon.png";

export const SelectAllButton = (props) => {
    return <button className="select-all-button" onClick={() => props.onSelect()}>
        <img className="select-all-img" src={select_all_icon} alt="Select All Button" />
        <h5>Select all</h5>
    </button>;
}