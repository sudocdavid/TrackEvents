import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.scss';
import logo_img from '../../res/images/logo_trackevents.png';
import { NavbarAuth } from "./NavbarAuth/NavbarAuth";
import { NavbarMenu } from "./NavbarMenu/NavbarMenu";
import { NavbarDropdownState } from '../../model/enums/NavbarDropdownState';
import { NavbarDropdownContainer } from './NavbarDropdownContainer';
import { DropdownLogin } from './dropdowns/DropdownLogin';
import { DropdownRegister } from './dropdowns/DropdownRegister';
import { DropdownFavorites } from './dropdowns/DropdownFavorites';
import { DropdownContacts } from './dropdowns/DropdownContacts';
import { DropdownSharedEvents } from './dropdowns/DropdownSharedEvents';
import { DropdownSettings } from './dropdowns/DropdownSettings';
import SessionDataController from '../../data/SessionDataController';

export const Navbar = (props) => {
    const [isUserSignedIn, setIsUserSignedIn] = useState(false);
    const [activeDropdownState, setActiveDropdownState] = useState(null);

    const navContent = (isUserSignedIn) => {
        if (isUserSignedIn) return <NavbarMenu setActiveDropdownState={(state) => setActiveDropdownState(state)} />
        return <NavbarAuth setActiveDropdownState={(state) => setActiveDropdownState(state)} currentDropdownState={activeDropdownState} />
    };

    const navDropdownContent = (dropdownState) => {
        // if scrollable dropdown list is needed: fill into list, return list
        if (dropdownState != null) {
            switch (dropdownState) {
                case NavbarDropdownState.Login:
                    return <DropdownLogin onSuccessfulLogin={data => onSuccessfulLogin(data)} onCreateNewAccount={() => setActiveDropdownState(NavbarDropdownState.Register)} />
                case NavbarDropdownState.Register:
                    return <DropdownRegister onSuccessfulRegister={() => onSuccessfulRegister()} />
                case NavbarDropdownState.Favourites:
                    return <DropdownFavorites />
                case NavbarDropdownState.Contacts:
                    return <DropdownContacts />
                case NavbarDropdownState.Messages:
                    return <DropdownSharedEvents />
                case NavbarDropdownState.Settings:
                    return <DropdownSettings onLogout={() => onLogout()} />
                default: return null;
            }
        }
    }

    const onSuccessfulLogin = (data) => {
        setActiveDropdownState(null);
        window.signedIn = true;
        setIsUserSignedIn(true);
        SessionDataController.setCountry(data.country);
    }

    const onSuccessfulRegister = (data) => {
        setActiveDropdownState(null);
        window.signedIn = true;
        setIsUserSignedIn(true);
    }

    const onLogout = () => {
        setActiveDropdownState(null);
        window.signedIn = false;
        setIsUserSignedIn(false);
    }

    return (
        <div id="navbar">
            <div id="navbar-left">
                <Link to='/'>
                    <img className='logo' src={logo_img} alt="" />
                </Link>
            </div>
            <div id="navbar-right">
                {navContent(isUserSignedIn)}
            </div>
            <NavbarDropdownContainer onClose={() => setActiveDropdownState(null)}>
                {navDropdownContent(activeDropdownState)}
            </NavbarDropdownContainer>
        </div>
    );
}