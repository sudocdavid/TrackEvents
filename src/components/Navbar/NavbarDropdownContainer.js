import './NavbarDropdownContainer.scss';
import { CloseButton } from '../CloseButton';

export const NavbarDropdownContainer = (props) => {
    if (props.children == null || props.children.length === 0) return <></>;
    return (
        <div className='navbar-dropdown-container'>
            <CloseButton onClose={() => props.onClose()} />
            {props.children}
        </div>
    );
}