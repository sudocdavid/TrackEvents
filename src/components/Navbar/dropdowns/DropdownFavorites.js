import React from 'react';
import { findFavorites } from '../../../data/database/axios';
import { getEventsData } from '../../../data/EventData';
import { FavDetailBox } from '../../cards/FavDetailBox';
import './DropdownItems.scss';


/**
 * Konnte aufgrund zeitlicher Beschränkungen leider nicht fertiggstellt werden
 */
export class DropdownFavorites extends React.Component {

    contactUserName = "";
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            favs: []
        }
    }

    async handleEventData() {
        const favList = [];
        const favsData = await findFavorites()
        if (favsData != null) {
            for (const fav of favsData.favorites) {
                const eventData = await getEventsData(fav);
                favList.push(<FavDetailBox eventData={eventData} />);
            }
            this.setState({ favs: favList });
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.handleEventData();
    }
    componentWillUnmount = () => this._isMounted = false;

    render() {
        return (
            <div className="dropdown-menu-item">
                <h4 className="dropdown-heading">Favorites</h4>
                <div className="favorites-detail-list">{this.state.favs}</div>
            </div >
        );
    }
}