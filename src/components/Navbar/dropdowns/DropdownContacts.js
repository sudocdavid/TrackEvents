import React from 'react';
import './DropdownItems.scss';

import { addContacts, findContacts, findUserID, getUserCountry, removeContact } from '../../../data/database/axios';
import { ContactsDetailView } from './dropdownView/ContactsDetailView';
const contactAddInputId = 'username-input';
const addButtonId = 'menu-dropdown-contacts-search-button';

export class DropdownContacts extends React.Component {

    contactUserName = "";
    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            userCountries: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
        findContacts().then(contactsDoc => {
            if (this._isMounted) return this.getUserCountries(contactsDoc.contacts);
        }).then(userCountries => this.setState({ userCountries: userCountries }));

        document.getElementById(contactAddInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') document.getElementById(addButtonId).click();
        });
    }
    componentWillUnmount = () => this._isMounted = false;

    onAddContactClick = async () => {
        document.getElementById(contactAddInputId).value = '';
        this.contactUserName = this.contactUserName.trim();
        if (this.contactUserName !== '') {
            const tempContactUserName = this.contactUserName;
            this.contactUserName = '';
            const contactOnlyId = await findUserID(tempContactUserName);
            if (contactOnlyId != null) {
                await addContacts(contactOnlyId._id)
                    .then(isSuccess => {
                        if (isSuccess) {
                            if (this.state.userCountries.find(userCountry => userCountry.name === tempContactUserName) != null) {
                                console.log("Contact already exists");
                            } else {
                                getUserCountry(contactOnlyId._id).then(userCountry => {
                                    if (userCountry != null) {
                                        this.state.userCountries.push(userCountry);
                                        this.setState({ userCountries: this.state.userCountries });
                                    }
                                });
                            }

                        } else console.log(isSuccess); // generate error view alert or similar
                    }).catch(err => console.log(err));
            } else alert("user doesn't exist")
        }
    }

    async getUserCountries(contacts) {
        const userCountries = [];
        if (contacts == null) return userCountries;
        for (const contactId of contacts) {
            const userCountry = await getUserCountry(contactId);
            if (userCountry != null) userCountries.push(userCountry);
        }
        return userCountries;
    }

    onRemoveContact(contactId) {
        removeContact(contactId);
        this.setState({ userCountries: this.state.userCountries.filter(userCountry => userCountry.contactId !== contactId) });
    }

    render() {
        return (
            <div className="dropdown-menu-item">
                <h4 className="dropdown-heading">Contacts</h4>
                <div>
                    <input id={contactAddInputId} className="dropdown-settings-input" placeholder="Username" onChange={e => this.contactUserName = e.target.value} />
                    <button id={addButtonId} onClick={() => this.onAddContactClick()}>Add</button>
                </div>
                <ContactsDetailView userCountries={this.state.userCountries} onRemoveContact={contactId => this.onRemoveContact(contactId)} />
            </div >
        );
    }
}