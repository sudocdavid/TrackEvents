import { useEffect } from 'react';
import './DropdownItems.scss';
import { changeUserPassword, changeUserCountry, deleteUser } from '../../../data/database/axios';

const changedPasswordInputId = 'password-input', changedCountryInputId = 'country-input';

export const DropdownSettings = (props) => {
    let userPassword;
    let userCountry;

    const onPasswordChangeClick = () => {
        changeUserPassword(userPassword)
            .then(document.getElementById(changedPasswordInputId).value = '', alert('User Password changed!')).catch(err => console.log(err));
    }

    const onCountryChangeClick = () => {
        changeUserCountry(userCountry)
            .then(document.getElementById(changedCountryInputId).value = '', alert('User Country changed!')).catch(err => console.log(err));
    }

    const onUserDeletedClick = () => {
        let isConfirmed = prompt('Do you really want to delete this account? Then confirm it with `yes`', null);
        if (isConfirmed != null) {
            deleteUser().then(alert('User deleted!')).catch(err => console.log(err));
            props.onLogout();
        }
    }

    useEffect(() => {
        document.getElementById(changedPasswordInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') onPasswordChangeClick();
        });
    })

    const onLogoutUser = () =>{
        props.onLogout();
        alert("you are logged out! See you next time");
    }

    return (
        <div className="dropdown-menu-item">
            <h4 className="dropdown-item-title">Settings</h4>
            <div className="settings-container">
                <input id={changedPasswordInputId} className="dropdown-settings-input" placeholder="New Password" onChange={e => userPassword = e.target.value} type="password" />
                <p id="hint-text">the password must contain at least 8 characters</p>
                <button id="menu-dropdown-settings-button" onClick={() => onPasswordChangeClick()}>Change Password</button>
            </div>
            <div className="settings-container">
                <input id={changedCountryInputId} className="dropdown-settings-input" placeholder="Preferred Country" onChange={e => userCountry = e.target.value} />
                <p id="hint-text">the country must be in 2-letter-code e.g. "DE" for Germany</p>
                <button id="menu-dropdown-settings-button" onClick={() => onCountryChangeClick()}>Change Preferred Country</button>
            </div>
            <div className="settings-container">
                <button id="menu-dropdown-settings-logout-button" onClick={() => onLogoutUser()}>Logout</button>
            </div>
            <div className="settings-container">
                <button id="menu-dropdown-settings-delete-button" onClick={() => onUserDeletedClick()}>Delete my Account</button>
            </div>
        </div>
    );
}