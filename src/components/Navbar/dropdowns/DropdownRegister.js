import { useEffect } from 'react';
import './DropdownAuth.scss';
import { loginUser, registerUser } from '../../../data/database/axios';

const usernameInputId = 'username-input', passwordInputId = 'password-input';

export const DropdownRegister = (props) => {
    let userName;
    let userPassword;

    const onRegisterClick = () => {
        var userNameInput = document.getElementById(usernameInputId).value;
        var userPasswordInput = document.getElementById(passwordInputId).value;

        if (userNameInput.length >= 1 && userNameInput.length <= 20 && userPasswordInput.length >= 8) {
            registerUser(userName, userPassword).then(status => {
                if (status === 200) loginUser(userName, userPassword).then(() => props.onSuccessfulRegister(userName));
                else if (status === 403) alert("user already exists");
            }).catch(err => console.log(err));
        } else alert("username or password has the wrong format")
    }

    useEffect(() => {
        document.getElementById(usernameInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') document.getElementById(passwordInputId).focus();
        });
        document.getElementById(passwordInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') onRegisterClick();
        });
    })

    return (
        <div className="dropdown-auth">
            <input id={usernameInputId} className="dropdown-auth-input" placeholder="Username" onChange={e => userName = e.target.value} />
            <p id="hint-text">the username should not contain more than 20 characters</p>
            <input id={passwordInputId} className="dropdown-auth-input" placeholder="Password" onChange={e => userPassword = e.target.value} type="password" />
            <p id="hint-text">the password must contain at least 8 characters</p>
            <button id="register-dropdown-button" className="auth-dropdown-button" onClick={() => onRegisterClick()}>Register</button>
        </div>
    );
}