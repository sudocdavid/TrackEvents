import { useEffect } from 'react';
import './DropdownAuth.scss';
import { loginUser } from '../../../data/database/axios.js';

const usernameInputId = 'username-input', passwordInputId = 'password-input';

export const DropdownLogin = (props) => {
    let userName;
    let userPassword;

    const onLoginClick = () => {
        loginUser(userName, userPassword).then(loginUserData => {
            if (loginUserData != null) {
                props.onSuccessfulLogin(loginUserData);
            } else alert('Combination of username and password is incorrect!');
        }).catch(err => console.log(err));

    }

    useEffect(() => {
        document.getElementById(usernameInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') document.getElementById(passwordInputId).focus();
        });
        document.getElementById(passwordInputId).addEventListener("keyup", (e) => {
            if (e.code === 'Enter') onLoginClick();
        });
    })

    return (
        <div className="dropdown-auth">
            <input id={usernameInputId} className="dropdown-auth-input" placeholder="Username" onChange={e => userName = e.target.value} />
            <input id={passwordInputId} className="dropdown-auth-input" placeholder="Password" onChange={e => userPassword = e.target.value} type="password" />
            <button id="login-dropdown-button" className="auth-dropdown-button" onClick={() => onLoginClick()}>Login</button>
            <div id="create-account-p">New to TrackEvents? <div onClick={() => props.onCreateNewAccount()}>Create an account</div></div>
        </div>
    );
}

