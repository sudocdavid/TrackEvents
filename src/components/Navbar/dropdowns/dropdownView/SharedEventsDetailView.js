import React from 'react';
import { SharedEventsBox } from '../../../cards/SharedEventsBox';
import "./ContactsDetailView.scss";

export class SharedEventsDetailView extends React.Component {

    render() {
        const sharedEventsBoxes = [];
        for (const userSharedEvents of this.props.userSharedEvents) {
            sharedEventsBoxes.push(<SharedEventsBox name={userSharedEvents.name} eventId={userSharedEvents.eventId} />)
        }

        return (
            <div class="sharedEvents-detail-view">
                {sharedEventsBoxes}
            </div>
        )
    }
}