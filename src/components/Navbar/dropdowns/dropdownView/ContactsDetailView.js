import React from 'react';
import { fetchCountriesData } from '../../../../data/CountryData';
import { ContactBox } from '../../../cards/ContactBox';
import "./ContactsDetailView.scss";

export class ContactsDetailView extends React.Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            countriesDataFiltered: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
        fetchCountriesData().then(data => {
            if (this._isMounted) this.setState({ countriesDataFiltered: this.countriesData = data })
        });
    }
    componentWillUnmount = () => this._isMounted = false;

    render() {
        const contactBoxes = [];
        for (const userCountry of this.props.userCountries) {
            const country = userCountry.country != null ? this.state.countriesDataFiltered.find(country => { return country.alpha2Code === userCountry.country.toUpperCase() }) : {flag: ""};
            contactBoxes.push(<ContactBox key={userCountry.name} name={userCountry.name} img_src={country.flag} contactId={userCountry.contactId} onDelete={() => this.props.onRemoveContact(userCountry.contactId)}/>)
        }

        return (
            <div className="contacts-detail-view">
                {contactBoxes}
            </div>
        )
    }
}