import React from 'react';
import './DropdownItems.scss';
import { findSharedEvents, getUserName } from '../../../data/database/axios';
import { SharedEventsDetailView } from './dropdownView/SharedEventsDetailView';


export class DropdownSharedEvents extends React.Component {

    _isMounted = false;

    constructor(props) {
        super(props);

        this.state = {
            userSharedEvents: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
        findSharedEvents().then(sharedEvents => {
            if (this._isMounted) return this.getSenderIds(sharedEvents);
        }).then(userSharedEvents => this.setState({ userSharedEvents: userSharedEvents }));
    }
    componentWillUnmount = () => this._isMounted = false;

    async getSenderIds(sharedEvents) {
        const userSharedEvents = [];
        for (const sharedEvent of sharedEvents) {
            const userNameData = await getUserName(sharedEvent.senderId.toString());
            if (userNameData != null) {
                userSharedEvents.push({ name: userNameData.name, eventId: sharedEvent.eventId })
            }
        }
        return userSharedEvents;
    }

    render() {

        return (
            <div className="dropdown-menu-item">

                <h4>Shared Events</h4>
                <div className="dropdown-item-title">You can share events with your contacts here</div>
                <SharedEventsDetailView userSharedEvents={this.state.userSharedEvents} />
            </div >
        );
    }
}