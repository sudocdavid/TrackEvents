import './NavbarAuth.scss';
import { NavbarDropdownState } from '../../../model/enums/NavbarDropdownState';

const loginButtonId = 'login-button', registerButtonId = 'register-button';

export const NavbarAuth = (props) => {
    const updateState = (newState) => {
        if (newState === props.currentDropdownState) props.setActiveDropdownState(null)
        else props.setActiveDropdownState(newState)
    }

    return (
        <div id='navbar-auth'>
            <button id={loginButtonId} className={props.currentDropdownState === NavbarDropdownState.Login ? "active-auth-button" : null} onClick={() => updateState(NavbarDropdownState.Login)}>
                Login
            </button>
            <button id={registerButtonId} className={props.currentDropdownState === NavbarDropdownState.Register ? "active-auth-button" : null} onClick={() => updateState(NavbarDropdownState.Register)}>
                Register
            </button>
        </div>
    );
}