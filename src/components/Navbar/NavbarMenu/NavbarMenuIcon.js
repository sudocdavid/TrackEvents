import './NavbarMenuIcon.scss'

export const NavbarMenuIcon = (props) => {
    return (
        <div className='nav-menu-icon' onClick={() => props.onClick()}>
            <img className="select-all-img" src={props.img} alt={props.img_alt} />
        </div>
    )
}