import { NavbarMenuIcon } from './NavbarMenuIcon';

import './NavbarMenu.scss';
import favorite_icon from "../../../res/images/menu_icons/grade_white_48dp.svg";
import people_icon from "../../../res/images/menu_icons/people_white_48dp.svg";
import notifications_icon from "../../../res/images/menu_icons/notifications_white_48dp.svg";
import settings_icon from "../../../res/images/menu_icons/settings_white_48dp.svg";
import { NavbarDropdownState } from '../../../model/enums/NavbarDropdownState';

export const NavbarMenu = (props) => {
    const navbarMenuIconConfig = [
        { img: favorite_icon, img_alt: "Favourites", dropdownState: NavbarDropdownState.Favourites },
        { img: people_icon, img_alt: "Contacts", dropdownState: NavbarDropdownState.Contacts },
        { img: notifications_icon, img_alt: "Messages", dropdownState: NavbarDropdownState.Messages },
        { img: settings_icon, img_alt: "Settings", dropdownState: NavbarDropdownState.Settings }
    ]

    const navbarMenuIcons = [];
    for (const conf of navbarMenuIconConfig) {
        navbarMenuIcons.push(<NavbarMenuIcon img={conf.img} img_alt={conf.img_alt} onClick={() => props.setActiveDropdownState(conf.dropdownState)} />)
    }

    return (
        <div className='navbar-menu'>
            <div className='navbar-menu-items'>
                {navbarMenuIcons}
            </div>
        </div>
    );
}