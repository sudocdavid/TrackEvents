import React from 'react';

import "./ImageChangeable.scss";

import EditRoundedIcon from '@material-ui/icons/EditRounded';

export class ImageChangeable extends React.Component {

    render() {
        return (
            <div className="flag-card">
                <img src={this.props.img_src} alt={this.props.img_alt} />
                <EditRoundedIcon className="edit-button" onClick={() => this.props.onClick()} />
            </div>
        );
    }
}