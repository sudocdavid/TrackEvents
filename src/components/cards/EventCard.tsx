// https://codepen.io/lukewheeler/pen/QeKzjM
// using sass: npm install node-sass

import React from 'react';
import "./EventCard.scss";
import favorite_icon from "../../res/images/menu_icons/grade_white_48dp.svg";
import placeholder_image from "../../res/images/placeholder.png";
import { EventData } from '../../model/EventData';
import { addFavorites } from '../../data/database/axios';

type Props = typeof EventCard.defaultProps & {
    eventData: EventData;
    onHover?: (isActive: boolean) => any;
    onSelect?: () => any;
}

interface State {
};

export class EventCard extends React.Component<Props, State> {
    static defaultProps = {
        onHover: (isActive: boolean) => console.log(isActive),
        onSelect: () => console.log("Selected")
    };

    onClick() {
        addFavorites(this.props.eventData.id);
    }

    render() {
        let eventData: EventData = this.props.eventData;

        return (
            <div className="card-selectable" id={eventData.id} onMouseEnter={() => this.props.onHover(true)} onMouseLeave={() => this.props.onHover(false)}>
                <div className="card-img-div">
                    <img
                        src={eventData.images[0]}
                        alt={placeholder_image}
                    />
                    <div className="img-overlay"></div>
                </div>
                <div className="card-body" onClick={() => this.cardClicked()}>
                    <h2>{eventData.title}</h2>
                    <p>{eventData.description}</p>
                    <p>{eventData.date}</p>
                    <h5>{eventData.location}</h5>
                </div>
                <div className="event-card-fav-button" onClick={() => this.onClick()}>
                    <img
                        className="event-card-fav-img"
                        src={favorite_icon}
                        alt=""
                        onClick={() => this.onClick()}
                    />
                </div>
            </div >
        )
    }

    cardClicked() {
        window.open(this.props.eventData.link, '_blank');
    }

}