// https://codepen.io/lukewheeler/pen/QeKzjM
// using sass: npm install node-sass

import React from 'react';
import "./CardSelectable.scss";
import { fetchImage, fetchRandomImage } from "../../data/ImageData.js";
import { ImageLinkButton } from '../ImageLinkButton';

export class CardSelectable extends React.Component {

    _isMounted = false;

    /**
     * Constructor of Card class
     * 
     * @param {*} props Any properties that can be applied to a card: title, id, img
     */
    constructor(props) {
        super(props);

        this.state = { img_src: props.cardModel.img_src };
    }

    componentDidMount() {
        this._isMounted = true;

        if (this.props.cardModel.fetch_img_query != null) {
            fetchImage(this.props.cardModel.fetch_img_query).then(img_src => {
                if (this._isMounted && img_src != null) this.setState({ img_src: img_src });
                else fetchImage(this.props.cardModel.fetch_img_query_alt).then(img_src => {
                    if (this._isMounted && img_src != null) this.setState({ img_src: img_src });
                    else fetchRandomImage().then(img_src => {
                        if (this._isMounted) this.setState({ img_src: img_src })
                    });
                });
            });
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    //TODO: only show button below if unsplash image
    render() {
        let cardModel = this.props.cardModel;

        return (
            <div className="card-selectable" id={cardModel.id} onClick={() => this.cardClicked()}>
                <div className="card-img-div">
                    <img src={this.state.img_src} alt="" />
                    <div className="img-overlay"></div>
                    <ImageLinkButton img_src={this.state.img_src} title="Show Image on Unsplash" />
                </div>
                <div className="card-body">
                    <h2>{cardModel.title}</h2>
                    <p>{cardModel.description}</p>
                    <h5>{cardModel.sub_description}</h5>
                </div>
            </div >
        )
    }

    cardClicked() {
        this.props.onSelect();
    }

}