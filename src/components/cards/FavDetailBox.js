import './FavDetailBox.scss';

export const FavDetailBox = (props) => {
    function openUrl() {
        if (props.eventData != null) {
            window.open(props.eventData.url)
        }
    }

    // TODO: add deletion of fav
    let name = "Favorite Event";
    if (props.eventData != null) name = props.eventData.name;
    return <div className="detail-event-button" onClick={() => openUrl()}>{name}</div>;
}