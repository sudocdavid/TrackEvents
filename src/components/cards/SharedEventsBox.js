import React from 'react';

import "./SharedEventsBox.scss";

export class SharedEventsBox extends React.Component {

    // TODO: fill with data
    render() {
        return (
            <div className="sharedEvents-box">
                <div className="card-body">
                    <p>{this.props.name}</p>
                </div>
                <div className="contact-box-country-img">
                    <p>{this.props.eventId} </p>
                </div>
            </div >
        )
    }
}