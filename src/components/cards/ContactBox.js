// https://codepen.io/lukewheeler/pen/QeKzjM
// using sass: npm install node-sass

import React from 'react';
import { DeleteButton } from '../DeleteButton';
import "./ContactBox.scss";

export class ContactBox extends React.Component {

    constructor(props) {
        super(props);

        this.state = { img_src: props.img_src };
    }

    render() {
        return (
            <div className="contact-box">
                <div className="card-body">
                    <div>{this.props.name}</div>
                </div>
                <div className="contact-box-country-img">
                    <img src={this.props.img_src} alt={this.props.img_alt} />
                </div>
                <DeleteButton onClick={() => this.props.onDelete()} />
            </div >
        )
    }


}