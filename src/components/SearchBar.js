import "./SearchBar.scss";

export const SearchBar = (props) => {
    const sqp = props.searchQueryProps;

    const checkInput = async (input) => {
        const filtered = sqp.data.filter(item => sqp.filterListItem(item, input));
        sqp.handleFilteredData(filtered);
    }

    if(sqp != null) return <input id={props.id} className="search-bar" placeholder={sqp.placeholder} onChange={(e) => checkInput(e.target.value)} />
    return <></>;
}

//<SearchBar input="" onChange={checkInput} placeholder={sqp.placeholder} />;