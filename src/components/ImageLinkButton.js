import { openImageSourceInNewTab } from '../data/ImageData.js';

import './ImageLinkButton.scss';

export const ImageLinkButton = (props) => {
    return <button className="image-link-button" onClick={() => openImageSourceInNewTab(props.img_src)}>{props.title}</button>;
}