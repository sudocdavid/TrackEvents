import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { withRouter } from "react-router";

import { HomePage } from './pages/home/HomePage';
import { EventSearchPage } from './pages/event-search/EventSearchPage';
import { Page404 } from './pages/Page404';
import { Navbar } from './components/Navbar/Navbar.js';

import './index.scss';

ReactDOM.render(
  <div id='main-div'>
    <Router>
      <Navbar />
      <Switch>
        <Route exact path={['/', '/home']} component={withRouter(HomePage)} />
        <Route path='/search' component={withRouter(EventSearchPage)} />
        <Route component={withRouter(Page404)} />
      </Switch>
    </Router>
    <div id='footer'>Footer</div>
  </div>,
  document.getElementById('root')
);
