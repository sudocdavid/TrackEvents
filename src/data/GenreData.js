import { CardData } from '../model/CardData';
import { TICKETMASTER_CONSUMER_KEY } from '../data/credentials';

const EVENTS_NUM = 100;

export async function fetchGenresData() {
    return await fetch(`https://app.ticketmaster.com/discovery/v2/classifications.json?apikey=${TICKETMASTER_CONSUMER_KEY}&size=${EVENTS_NUM}`)
        .then(response => response.json())
        .then(data => data._embedded.classifications)
        .catch(error => console.warn(error));
}

export function convertData_GenresToCards(genresData) {
    const cardsData = [];
    if (genresData == null) return cardsData;
    for (const genreData of genresData) {
        const type = genreData.type != null ? "type" : (genreData.segment != null ? "segment" : null);
        if (type != null) {
            const data = type === "type" ? genreData.type : genreData.segment;
            const title = data.name;
            if (title === "Undefined") continue; // "Undefined" is correct. It's an undefined genre name, but shall not be included
            let subGenres = "";
            const subGenresData = type === "type" ? data._embedded.subtypes : data._embedded.genres;
            for (const subGenre of subGenresData) {
                if (subGenres.length > 0) subGenres += ", ";
                subGenres += subGenre.name;
                if (subGenres.length > 64 && subGenresData.indexOf(subGenre) !== subGenresData.length - 1) {
                    subGenres += ", ..."
                    break;
                }
            }
            const countryCardModel = new CardData(
                type + '-' + title,
                title,
                title,
                subGenres,
                type,
                "",
                "",
                title,
                data,
                subGenres
            )
            cardsData.push(countryCardModel);
        }
    }
    return cardsData;
}

export function getGenreRootNode(genreItem) {
    return genreItem.type != null ? genreItem.type : (genreItem.segment != null ? genreItem.segment : null);
}