import { getSHA256ofString } from './hashing';

const axios = require('axios');
axios.defaults.baseURL = 'https://gitlab.com/sudocdavid/trackevents-backend.git';
const urlHost = "http://localhost:4000/";
var userID = null;

/* user requests */
export async function loginUser(username, userpassword) {
    return await axios.post(urlHost + 'users/login', { name: username, password: getSHA256ofString(userpassword) })
        .then(res => {
            if (res.status === 200) {
                userID = res.data._id;
                return res.data;
            } else console.log('login failed status: ' + res.status);
            return null;
        })
        .catch(err => console.log(err));
}

export async function getUserCountry(contactId) {
    return await axios.get(urlHost + 'users/get/' + contactId)
        .then(res => {
            if (res.status === 200) return { name: res.data.name, country: res.data.country, contactId: contactId };
            console.log('not found, status: ' + res.status);
            return null;
        })
        .catch(err => {
            return null;
        });
}

export async function findUserID(username) {
    return await axios.get(urlHost + 'users/findUserId?name=' + username)
        .then(res => {
            if (res.status === 200) {
                return res.data;
            } else console.log('not found, status: ' + res.status);
            return null;
        })
        .catch(err => console.log(err));
}

export async function getUserName(id) {
    return await axios.get(urlHost + 'users/get/' + id)
        .then(res => {
            if (res.status === 200) return { name: res.data.name };
            else console.log('not found, status: ' + res.status);
            return null;
        })
        .catch(err => console.log(err));
}

export async function changeUserPassword(password) {
    return await axios.post(urlHost + 'users/changeP/' + userID, { password: getSHA256ofString(password) })
        .then(res => console.log(res))
        .catch(err => console.log(err));
}

export async function changeUserCountry(country) {
    if (userID != null) {
        return await axios.post(urlHost + 'users/changeC/' + userID, { country: country })
            .then(res => { return true })
            .catch(err => console.log(err));
    }
    return false;
}

// returns true if user got registered sucessfully
export async function registerUser(username, userpassword) {
    return axios.post(urlHost + 'users/add', { name: username, password: getSHA256ofString(userpassword) })
        .then(res => res.status)
        .catch(err => console.log(err))
}

export async function deleteUser() {
    return await axios.post(urlHost + 'users/remove/' + userID)
        .then(res => console.log(res))
        .catch(err => console.log(err));
}

/* contacts requests */
export async function findContacts() {
    return await axios.get(urlHost + 'contacts/' + userID)
        .then(res => { if (res.status === 200) return res.data })
        .catch(err => console.log(err));
}

export async function initContacts() {
    return await axios.post(urlHost + 'contacts/init/' + userID,)
        .then(res => console.log(res))
        .catch(err => console.log(err));
}

export async function addContacts(contactID) {
    if (userID === contactID) return false;
    return await axios.post(urlHost + 'contacts/add/' + userID, { contactId: contactID }).then(res => true)
        .catch(err => err);
}

export async function removeContact(contactID) {
    return await axios.post(urlHost + 'contacts/remove/' + userID, { contactId: contactID }).then(res => true)
        .catch(err => err);
}

/* favorites requests */
export async function findFavorites() {
    return await axios.get(urlHost + 'favorites/' + userID)
        .then(res => {
            if (res.status === 200) {
                //console.log('there are favorites: ' + res.status);
                return res.data;
            }
        })
        .catch(err => console.log(err));
}

export async function initFavorites() {
    return await axios.post(urlHost + 'favorites/init/' + userID,)
        .then(res => console.log(res))
        .catch(err => console.log(err));
}

export async function addFavorites(favEventID) {
    return await axios.post(urlHost + 'favorites/add/' + userID, { favEventId: favEventID }).then(res => console.log(res))
        .catch(err => console.log(err));
}

/* sharedEvents requests */
export async function findSharedEvents() {
    return await axios.get(urlHost + 'sharedEvents/' + userID)
        .then(res => {
            if (res.status === 200) {
               // console.log('tehre are shared events: ' + res.status + res.data);
                return res.data;
            }
        })
        .catch(err => console.log(err));
}

export async function addSharedEvents(eventID) {
    return await axios.post(urlHost + 'sharedEvents/add/' + userID, { eventId: eventID }).then(res => console.log(res))
        .catch(err => console.log(err));
}
