import crypto from 'crypto';
export const getSHA256ofString = (str) => crypto.createHash('sha256').update(str).digest('hex');
export const getSHA256ofJSON = (input) => crypto.createHash('sha256').update(JSON.stringify(input)).digest('hex');
