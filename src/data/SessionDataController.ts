const SESSION_KEY_COUNTRY = "country";
const SESSION_KEY_GENRE = "genre";
const SESSION_KEY_GENRE_CLASS = "genreclass"; //either typeId or segmentId

export default class SessionDataController {
    static setCountry = (alpha2Code: any) => sessionStorage.setItem(SESSION_KEY_COUNTRY, alpha2Code);
    static getCountry = () => { return sessionStorage.getItem(SESSION_KEY_COUNTRY) };
    static setGenre = (genre: any) => sessionStorage.setItem(SESSION_KEY_GENRE, genre);
    static getGenre = () => { return sessionStorage.getItem(SESSION_KEY_GENRE) };
    static setGenreClass = (genreClass: any) => sessionStorage.setItem(SESSION_KEY_GENRE_CLASS, genreClass);
    static getGenreClass = () => { return sessionStorage.getItem(SESSION_KEY_GENRE_CLASS) };
}
