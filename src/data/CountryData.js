import { CardData } from '../model/CardData';
import placeholder_image from "../res/images/placeholder.png";

const COUNTRIES_API_URL = "https://restcountries.eu/rest/v2/all";

export async function fetchCountriesData() {
    return await fetch(COUNTRIES_API_URL)
        .catch(err => console.warn("Data from", COUNTRIES_API_URL, "could not bet fetched.\n", err))
        .then(response => response?.json() || [])
}

export function convertData_CountriesToCards(countriesData) {
    const cardsData = [];
    for (const countryData of countriesData) {
        const countryCardModel = new CardData(
            countryData.name + countryData.alpha2Code,
            "card-" + countryData.name + "-" + countryData.alpha2Code,
            countryData.name,
            countryData.nativeName,
            "Find events in " + countryData.name,
            "", // no href yet
            placeholder_image, // no pre-loaded img
            countryData.name,
            countryData
        )
        cardsData.push(countryCardModel);
    }
    return cardsData;
}

export function sortDataByPopulation(data) {
    return data.sort((a, b) => {
        if (a.population < b.population) {
            return 1;
        } else if (a.population > b.population) {
            return -1;
        }
        return 0;
    });
}

export function filterCountryItems(item, input) {
    if (item == null) return false;
    return item.name.toLowerCase().includes(input.toLowerCase())
}