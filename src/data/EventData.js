import axios from "axios";
import { EventData } from "../model/EventData";
import { TICKETMASTER_CONSUMER_KEY } from "./credentials";

const EVENTS_NUM = 100;
//Problem:  Länder von Ticketmaster prio 2-3
//          Filter auf mehrere Genres und mehrere Country

export async function fetchEventsData(countryCode, genreData) {
    let params = { apikey: TICKETMASTER_CONSUMER_KEY, size: EVENTS_NUM };
    if (countryCode != null && countryCode !== "null" && countryCode !== "undefined") params["countryCode"] = countryCode;
    if (genreData != null) {
        if (genreData.typeId != null && genreData.typeId !== "null" && genreData.typeId !== "undefined") params["typeId"] = genreData.typeId;
        if (genreData.segmentId != null && genreData.segmentId !== "null" && genreData.segmentId !== "undefined") params["segmentId"] = genreData.segmentId;
    }
    return await axios.get("https://app.ticketmaster.com/discovery/v2/events.json", { params: params })
        .then(res => res?.data?._embedded?.events?.filter(event => event.dates.status.code !== 'cancelled') || null)
        .catch(error => console.warn(error));
}

export async function getEventsData(id) {
    let params = { apikey: TICKETMASTER_CONSUMER_KEY };
    return await axios.get("https://app.ticketmaster.com/discovery/v2/events/" + id + ".json", { params: params })
        .then(res => res.data)
        .catch(error => console.warn(error));
}

// TODO: add alternative to CardData model for EventCards and append as elements below
export function convertData_EventsToCardsData(eventsData) {
    const cardsData = [];
    if (eventsData == null) return cardsData;
    for (const eventData of eventsData) {
        let venue = "";
        if (eventData?._embedded?.venues != null) venue = eventData?._embedded?.venues[0]?.name;
        const eventCardDataModel = new EventData(
            eventData.id,
            eventData.id,
            eventData.name,
            eventData.dates.timezone,
            eventData.dates.startDateTime,
            venue,
            eventData.url,
            eventData.images.map(imageObj => imageObj.url),
            eventData
        )
        cardsData.push(eventCardDataModel);
    }
    return cardsData;
}