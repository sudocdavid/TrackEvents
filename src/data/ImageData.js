import { UNPLASH_ACCESS_KEY } from '../data/credentials';

const IMG_ORITENTATION = "landscape";
const IMG_SIZE = "small";
const IMG_NUM = 1;    // randomizes between IMAGE_NUM unsplash pictures

const IMAGES = [];

export async function fetchImage(query, query_alt) {
    if (IMAGES[query] != null) return IMAGES[query];
    return fetch(`https://api.unsplash.com/search/photos?client_id=${UNPLASH_ACCESS_KEY}&query=${query}&orientation=${IMG_ORITENTATION}&per_page=${IMG_NUM}`)
        .then(response => response.json())
        .then(data => {
            const num = Math.floor(Math.random() * data.results.length);
            if (data.results[num] == null) return null;
            //const url = getDataWithErrHandling(data, ["results", num, "urls", IMG_SIZE]);
            const url = data?.results[num]?.urls[IMG_SIZE];
            IMAGES[query] = url;
            return url;
        })
        .catch(error => {
            console.warn(error);
            if (query_alt != null) return fetchImage(query_alt);
            return null;
        });
}

export async function fetchImages(queries) {
    let images = [];
    for (let query of queries) {
        await fetchImage(query).then(img => {
            images.push(img);
        });
    }
    return images;
}

export async function fetchRandomImage() {
    return fetch(`https://api.unsplash.com/photos/random?client_id=${UNPLASH_ACCESS_KEY}&orientation=${IMG_ORITENTATION}&per_page=${IMG_NUM}`)
        .then(response => response.json())
        .then(data => data?.urls[IMG_SIZE])
        .catch(error => console.warn(error));
}

export function openImageSourceInNewTab(src) {
    window.open(src, '_blank');
}
